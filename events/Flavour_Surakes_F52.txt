
namespace = surakes_missions

#A Capital for Our Nation
country_event = {
	id = surakes_missions.1
	title = surakes_missions.1.t
	desc = surakes_missions.1.d
	picture = CITY_VIEW_eventPicture
	
	is_triggered_only = yes
	
	#Keep Current Capital
	option = {
		name = surakes_missions.1.a
		capital_scope = {
			add_province_modifier = {
				name = surakes_capital_modifier
				duration = 3650
			}
		}
		add_prestige = 15
	}
	
	#Azka-szel-Azka
	option = {
		name = surakes_missions.1.b
		trigger = { NOT = { capital = 522 } }
		
		set_capital = 522
		522 = {
			add_province_modifier = {
				name = surakes_capital_modifier
				duration = 3650
			}
		}
		add_prestige = 5
	}
	
	#Aqatbar
	option = {
		name = surakes_missions.1.c
		trigger = { NOT = { capital = 536 } }
		
		set_capital = 536
		536 = {
			add_province_modifier = {
				name = surakes_capital_modifier
				duration = 3650
			}
		}
		add_prestige = 5
	}
	
	#Bulwar
	option = {
		name = surakes_missions.1.h
		trigger = { NOT = { capital = 601 } }
		
		set_capital = 601
		601 = {
			add_province_modifier = {
				name = surakes_capital_modifier
				duration = 3650
			}
		}
		add_prestige = 5
	}
	
	#Azka-Sur
	option = {
		name = surakes_missions.1.e
		trigger = { NOT = { capital = 643 } }
		
		set_capital = 643
		643 = {
			add_province_modifier = {
				name = surakes_capital_modifier
				duration = 3650
			}
		}
		add_prestige = 5
	}
	
	#Brasan
	option = {
		name = surakes_missions.1.f
		trigger = { NOT = { capital = 565 } }
		
		set_capital = 565
		565 = {
			add_province_modifier = {
				name = surakes_capital_modifier
				duration = 3650
			}
		}
		add_prestige = 5
	}
	
	#Eduz-Vacyn
	option = {
		name = surakes_missions.1.g
		trigger = { NOT = { capital = 631 } }
		
		set_capital = 631
		631 = {
			add_province_modifier = {
				name = surakes_capital_modifier
				duration = 3650
			}
		}
		add_prestige = 5
	}
}

#Professionalize the State
country_event = {
	id = surakes_missions.2
	title = surakes_missions.2.t
	desc = surakes_missions.2.d
	picture = UNIVERSITY_eventPicture
	
	is_triggered_only = yes
	
	#Vacyntassi Education
	option = {
		name = surakes_missions.2.a
		trigger = { not = { has_idea_group = F31_ideas } }
		
		add_country_modifier = {
			name =  surakes_education1_modifier
			duration = -1
		}
	}
	
	#Azka-Surian Education	
	option = {
		name = surakes_missions.2.b
		trigger = { not = { has_idea_group = U20_ideas } }
		
		add_country_modifier = {
			name =  surakes_education2_modifier
			duration = -1
		}
	}
	
	#Azkabari Education	
	option = {
		name = surakes_missions.2.c
		trigger = { not = { has_idea_group = F35_ideas } }

		add_country_modifier = {
			name =  surakes_education3_modifier
			duration = -1
		}
	}
	
	#Akalsesi Education	
	option = {
		name = surakes_missions.2.p
		trigger = { not = { has_idea_group = F33_ideas } }
		
		add_country_modifier = {
			name =  surakes_education4_modifier
			duration = -1
		}
	}
	
	#Kumarkandi Education	
	option = {
		name = surakes_missions.2.e
		trigger = { not = { has_idea_group = F30_ideas } }
		
		add_country_modifier = {
			name =  surakes_education5_modifier
			duration = -1
		}
	}
	
	#Zambari Education	
	option = {
		name = surakes_missions.2.f
		trigger = { not = { has_idea_group = F41_ideas } }
		
		add_country_modifier = {
			name =  surakes_education6_modifier
			duration = -1
		}
	}
	
	#Bulwari Education	
	option = {
		name = surakes_missions.2.g
		trigger = { not = { has_idea_group = F39_ideas } }
		
		add_country_modifier = {
			name =  surakes_education7_modifier
			duration = -1
		}
	}
	
	#Kalibi Education	
	option = {
		name = surakes_missions.2.h
		trigger = { not = { has_idea_group = F44_ideas } }
		
		add_country_modifier = {
			name =  surakes_education8_modifier
			duration = -1
		}
	}
	
	#Brasanni Education	
	option = {
		name = surakes_missions.2.i
		trigger = { not = { has_idea_group = F24_ideas } }
		
		add_country_modifier = {
			name =  surakes_education9_modifier
			duration = -1
		}
	}
	
	#Dartaxagerdi Education	
	option = {
		name = surakes_missions.2.j
		trigger = { not = { has_idea_group = F22_ideas } }
		
		add_country_modifier = {
			name =  surakes_education10_modifier
			duration = -1
		}
	}
	
	#Gelkali Education	
	option = {
		name = surakes_missions.2.k
		trigger = { not = { has_idea_group = F26_ideas } }
		
		add_country_modifier = {
			name =  surakes_education11_modifier
			duration = -1
		}
	}
	
	#Re'uyeli Education	
	option = {
		name = surakes_missions.2.l
		trigger = { not = { has_idea_group = U18_ideas } }
		
		add_country_modifier = {
			name =  surakes_education12_modifier
			duration = -1
		}
	}
	
	#Bahari Education	
	option = {
		name = surakes_missions.2.m
		
		add_country_modifier = {
			name =  surakes_education13_modifier
			duration = -1
		}
	}
	
	#Zanite Education	
	option = {
		name = surakes_missions.2.n
		
		add_country_modifier = {
			name =  surakes_education14_modifier
			duration = -1
		}
	}
	
	#Surani Education	
	option = {
		name = surakes_missions.2.o
		
		add_country_modifier = {
			name =  surakes_education15_modifier
			duration = -1
		}
	}
	
	#Elven Education
	option = {
		name = surakes_missions.2.r
		trigger = { mission_completed = surakes_found_sun }
		
		add_country_modifier = {
			name =  surakes_education16_modifier
			duration = -1
		}
	}
		
	#Dwarven Education
	option = {
		name = surakes_missions.2.q
		trigger = { mission_completed = surakes_looking_cooper }
		
		add_country_modifier = {
			name =  surakes_education17_modifier
			duration = -1
		}
	}
}	

#An Anthem for Surakes
country_event = {
	id = surakes_missions.3
	title = surakes_missions.3.t
	desc = surakes_missions.3.d
	picture = RELIGIOUS_CONVERSION_eventPicture

	is_triggered_only = yes
	
	#The Glory of the Sun God
	option = {
		name = surakes_missions.3.a
		
		country_event = { id = surakes_missions.17 }
	}
		
	#The Legacy of Karqaslu
	option = {
		name = surakes_missions.3.b
		trigger = { 
			OR = { 	
				was_tag = F30 #Kumarkand
				has_country_modifier = surakes_lessons2_modifier #Sword of the Last King of Karqaslu
			} 
		}
		country_event = { id = surakes_missions.18 }
	}
		
	#The Trade Networks of Bulwar	
	option = {
		name = surakes_missions.3.c
		
		country_event = { id = surakes_missions.19 }
	}
		
	#The Beauty of our Gardens
	option = {
		name = surakes_missions.3.e
		
		country_event = { id = surakes_missions.20 }
	}
		
	#The Gifts of Brasan the Emancipator
	option = {
		name = surakes_missions.3.f
		
		country_event = { id = surakes_missions.21 }
	}
		
	#The Fight for our Independence
	option = {
		name = surakes_missions.3.g
		trigger = { religion = old_bulwari_sun_cult } 
		
		country_event = { id = surakes_missions.22 }
	}
		
	#The Deeds of Jaher
	option = {
		name = surakes_missions.3.h
		trigger = { religion = bulwari_sun_cult } 
		
		country_event = { id = surakes_missions.23 }
	}
		
	#The Revelations of Jaddar	
	option = {
		name = surakes_missions.3.i
		trigger = { religion = the_jadd } 
		
		country_event = { id = surakes_missions.24 }
	}
}
	
#We Need a Hero
country_event = {
	id = surakes_missions.4
	title = surakes_missions.4.t
	desc = surakes_missions.4.d
	picture = TOURNAMENT_eventPicture

	is_triggered_only = yes

	#Brasan the Emancipator
	option = {
		name = surakes_missions.4.a
		
		add_country_modifier = {
			name =  surakes_hero1_modifier
			duration = -1
		}
	}
		
	#Batur II of Karqaslu
	option = {
		name = surakes_missions.4.b
		trigger = { 
			OR = { 	
				was_tag = F30 #Kumarkand
				has_country_modifier = surakes_lessons2_modifier #Sword of the Last King of Karqaslu
			} 
		}		
		add_country_modifier = {
			name =  surakes_hero2_modifier
			duration = -1
		}
	}
		
	#Jaher
	option = {
		name = surakes_missions.4.c
		trigger = { religion = bulwari_sun_cult } 
		
		add_country_modifier = {
			name =  surakes_hero3_modifier
			duration = -1
		}
	}
		
	#Dartaxes
	option = {
		name = surakes_missions.4.e
		trigger = { religion = old_bulwari_sun_cult } 
		
		add_country_modifier = {
			name =  surakes_hero4_modifier
			duration = -1
		}
	}
		
	#Taelarios
	option = {
		name = surakes_missions.4.f
		trigger = { religion = bulwari_sun_cult } 
		
		add_country_modifier = {
			name =  surakes_hero5_modifier
			duration = -1
		}
	}
		
	#Jaddar
	option = {
		name = surakes_missions.4.g
		trigger = { religion = the_jadd } 
		
		add_country_modifier = {
			name =  surakes_hero7_modifier
			duration = -1
		}
	}
		
	#Barid-szel-Sur
	option = {
		name = surakes_missions.4.h
		trigger = {
			or = {
				was_tag = F45
				was_tag = U20
			}
		}
		
		add_country_modifier = {
			name =  surakes_hero6_modifier
			duration = -1
		}
	}
		
	#Castan the Progenitor
	option = {
		name = surakes_missions.4.i
		trigger = { capital_scope = { region = bahar_region } }
		
		add_country_modifier = {
			name =  surakes_hero8_modifier
			duration = -1
		}
	}
	
	#Erlian Surzuir
	option = {
		name = surakes_missions.4.j
		trigger = {
			or = {
				was_tag = F45
				was_tag = U20
			}
		}
		
		add_country_modifier = {
			name =  surakes_hero9_modifier
			duration = -1
		}
	}
	
	#Hammura the Wise
	option = {
		name = surakes_missions.4.k
		trigger = {
			or = {
				religion = old_bulwari_sun_cult
				was_tag = F31
			}
		}
		
		add_country_modifier = {
			name =  surakes_hero10_modifier
			duration = -1
		}
	}
}

#Daughters of Firanya
country_event = {
	id = surakes_missions.5
	title = surakes_missions.5.t
	desc = surakes_missions.5.d
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes

	#The land belongs to the Bulwari
	option = {
	name = surakes_missions.5.a
		
		every_province = {
			limit = { 
				superregion = bulwar_superregion
				or = {
					has_harpy_minority_trigger = yes
					has_harpy_majority_trigger = yes
				}	
			}
			add_province_modifier = {
				name =  surakes_colonies_modifier
				duration = 9125
			}
		}
	}
		
	#They can keep their lands
	option = {
	name = surakes_missions.5.b
		
		every_province = {
			limit = { 
				superregion = bulwar_superregion
				or = {
					has_harpy_minority_trigger = yes
					has_harpy_majority_trigger = yes
				}	
			}
			add_province_modifier = {
				name =  surakes_land_modifier
				duration = 9125
			}
		}
		set_country_flag = surakes_harpy_rights
	}
}

#Deliver Us
country_event = {
	id = surakes_missions.6
	title = surakes_missions.6.t
	desc = surakes_missions.6.d
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes

	#The land belongs to the Bulwari
	option = {
	name = surakes_missions.5.a
		
		every_province = {
			limit = { 
				superregion = bulwar_superregion
				or = {
					has_goblin_minority_trigger = yes
					has_goblin_majority_trigger = yes
				}	
			}
			add_province_modifier = {
				name =  surakes_colonies_modifier
				duration = 9125
			}
		}
	}
		
	#They can keep their lands
	option = {
	name = surakes_missions.5.b
		
		every_province = {
			limit = { 
				superregion = bulwar_superregion
				or = {
					has_goblin_minority_trigger = yes
					has_goblin_majority_trigger = yes
				}	
			}
			add_province_modifier = {
				name =  surakes_land_modifier
				duration = 9125
			}
		}
		set_country_flag = surakes_goblin_rights
	}
}

#The Gnoll Question
country_event = {
	id = surakes_missions.7
	title = surakes_missions.7.t
	desc = surakes_missions.7.d
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes

	#The land belongs to the Bulwari
	option = {
	name = surakes_missions.5.a
		
		every_province = {
			limit = { 
				superregion = bulwar_superregion
				or = {
					has_gnollish_minority_trigger = yes
					has_gnollish_majority_trigger = yes
				}	
			}
			add_province_modifier = {
				name =  surakes_colonies_modifier
				duration = 9125
			}
		}
	}
		
	#They can keep their lands
	option = {
	name = surakes_missions.5.b
		
		every_province = {
			limit = {
				superregion = bulwar_superregion
				or = {
					has_gnollish_minority_trigger = yes
					has_gnollish_majority_trigger = yes
				}	
			}
			add_province_modifier = {
				name =  surakes_land_modifier
				duration = 9125
			}
		}
		set_country_flag = surakes_gnoll_rights
	}
}

#What is a Bulwari?
country_event = {
	id = surakes_missions.8
	title = surakes_missions.8.t
	desc = surakes_missions.8.d
	picture = CULTURE_eventPicture

	is_triggered_only = yes

	#All who want to live in our country
	option = {
	name = surakes_missions.8.a
		
		if = { 	
			limit = { has_country_flag = surakes_harpy_rights } 
			every_owned_province = {
				limit = { 
					culture_group = harpy
					superregion = bulwar_superregion 
				}
				add_province_modifier = {
					name = surakes_nationalism_modifier
					duration = -1
				} 
			}
			capital_scope = {
				add_harpy_minority_size_effect = yes
			}
		}
		
		if = { 	
			limit = { has_country_flag = surakes_goblin_rights } 
			every_owned_province = {
				limit = { 
					culture_group = goblin
					superregion = bulwar_superregion
				}
				add_province_modifier = {
					name = surakes_nationalism_modifier
					duration = -1
				}
				capital_scope = {
					add_goblin_minority_size_effect = yes
				}
			}
		}
					
		if = { 	
			limit = { has_country_flag = surakes_gnoll_rights } 
			every_owned_province = {
				limit = { 
					culture_group = gnollish 
					superregion = bulwar_superregion 
				}
				add_province_modifier = {
					name = surakes_nationalism_modifier
					duration = -1
				}
				capital_scope = {
					add_gnollish_minority_size_effect = yes
				}		
			}
		}
		
		if = {
			limit = {
				has_country_flag = surakes_goblin_rights
				NOT = { accepted_culture = exodus_goblin }
			}
			add_accepted_culture = exodus_goblin
		}
		
		if = {
			limit = {
				has_country_flag = surakes_harpy_rights
				NOT = { accepted_culture = firanyan_harpy }
			}
				add_accepted_culture = firanyan_harpy
		}
		
		if = {
			limit = {
				has_country_flag = surakes_harpy_rights
				NOT = { accepted_culture = siadunan_harpy }
			}
			add_accepted_culture = siadunan_harpy
		}
		
		if = {
			limit = {
				has_country_flag = surakes_gnoll_rights
				NOT = { accepted_culture = east_sandfang_gnoll }
			}
			add_accepted_culture = east_sandfang_gnoll
		}
	}
	
	#Those monsters have no place in Surake�
	option = {
	name = surakes_missions.8.b
		trigger = {  
			NOT = { religion = the_jadd }
			NOT = { has_country_flag = surakes_gnoll_rights }
			NOT = { has_country_flag = surakes_harpy_rights }
			NOT = { has_country_flag = surakes_goblin_rights }
		}				
				
		if = {
			limit = { NOT = { has_country_flag = surakes_gnoll_rights } }
			country_event = { id = racial_pop_events_gnollish.13 } 	
		}
		if = {
			limit = { NOT = { has_country_flag = surakes_harpy_rights } }
			country_event = { id = racial_pop_events_harpy.13 }
		}
		if = {
			limit = { NOT = { has_country_flag = surakes_goblin_rights } }
			country_event = { id = racial_pop_events_goblin.13 } 
		}	
	}
}

#The Garden of Surake�
country_event = {
	id = surakes_missions.9
	title = surakes_missions.9.t
	desc = surakes_missions.9.d
	picture = CITY_VIEW_eventPicture

	is_triggered_only = yes

	option = {
		name = surakes_missions.9.a
		suran_river = {
			add_permanent_province_modifier = {
				name = jaddari_blessed_land
				duration = -1
			}
		custom_tooltip = desc_jaddari_blessed_land
		}
	}
}

#Gardens in the River
country_event = {
	id = surakes_missions.10
	title = surakes_missions.10.t
	desc = surakes_missions.10.d
	picture = CITY_DEVELOPMENT_eventPicture

	is_triggered_only = yes
	
	option = {
	name = surakes_missions.10.a
	
		surakes_river_provinces = { 
			if = {
				limit = { 
					not = { has_province_modifier = surakes_paradise_garden } 
				}
				add_permanent_province_modifier = {
					name = surakes_paradise_garden
					duration = -1
				}
			}
		}
	}
}

#Gardens in the Mountain
country_event = {
	id = surakes_missions.11
	title = surakes_missions.11.t
	desc = surakes_missions.11.d
	picture = CITY_DEVELOPMENT_eventPicture

	is_triggered_only = yes

	option = {
	name = surakes_missions.10.a
	
		surakes_desert_provinces = { 
			if = {
				limit = { 
					not = { has_province_modifier = surakes_paradise_garden } 
				}
				add_permanent_province_modifier = {
					name = surakes_paradise_garden
					duration = -1
				}
			}
		}
	}
}

#Gardens in the Desert
country_event = {
	id = surakes_missions.12
	title = surakes_missions.12.t
	desc = surakes_missions.12.d
	picture = CITY_DEVELOPMENT_eventPicture

	is_triggered_only = yes

	option = {
	name = surakes_missions.10.a
	
		surakes_mountain_provinces = { 
			if = {
				limit = { 
					not = { has_province_modifier = surakes_paradise_garden } 
				}
				add_permanent_province_modifier = {
					name = surakes_paradise_garden
					duration = -1
				}
			}
		}
	}
}

#The Earthly Paradise
country_event = {
	id = surakes_missions.13
	title = surakes_missions.13.t
	desc = surakes_missions.13.d
	picture = GREAT_BUILDING_eventPicture

	is_triggered_only = yes

	option = {
	name = surakes_missions.13.a
	
		601 = { 
			if = {
				limit = { 
					not = { has_province_modifier = surakes_sun_garden } 
				}
				add_permanent_province_modifier = {
					name = surakes_sun_garden
					duration = -1
				}
			}
		}
	}
}

#Lessons from Our Past
country_event = {
	id = surakes_missions.14
	title = surakes_missions.14.t
	desc = surakes_missions.14.d
	picture =BIG_BOOK_eventPicture

	is_triggered_only = yes

	option = {
	name = surakes_missions.14.a
	
		country_event = {id = surakes_missions.26 days = 1825 }
	}
	
	option = {
	name = surakes_missions.14.b
	
		country_event = {id = surakes_missions.27 days = 1825 }
	}
	
	option = {
	name = surakes_missions.14.c
	
		country_event = {id = surakes_missions.25 days = 1825 }
	}
	
	option = {
	name = surakes_missions.14.e
	
		country_event = {id = surakes_missions.15 days = 1825 }
	}
}

#Lessons from Our Past - Reward Brasan
country_event = {
	id = surakes_missions.15
	title = surakes_missions.15.t
	desc = surakes_missions.15.d
	picture = BIG_BOOK_eventPicture

	is_triggered_only = yes

	option = {
		name = surakes_missions.15.a
		random_list = {
			15 = {
				565 = { change_trade_goods = damestear }
			}
			15 = {
				565 = { change_trade_goods = precursor_relics }
			}
			50 = {
				add_treasury = 1500
				add_prestige = 25
			}
			20 = {
				add_country_modifier = {
					name = surakes_lessons4_modifier #Deep Devil Remains
					duration = -1
				}
			}
		}
	}
}

#The Solstice Festival
country_event = {
	id = surakes_missions.16
	title = surakes_missions.16.t
	desc = surakes_missions.16.d
	picture = FEAST_eventPicture

	is_triggered_only = yes

	option = {
	name = surakes_missions.16.a
	
		add_country_modifier = {
			name = surakes_solstice_modifier
			duration = 9125
		}
	}
}

#Anthem: The Glory of the Sun God
country_event = {
	id = surakes_missions.17
	title = surakes_missions.17.t
	desc = surakes_missions.17.d
	picture = STREET_SPEECH_eventPicture

	is_triggered_only = yes
	
	option = {
	name = surakes_missions.17.a
		add_country_modifier = {
			name =  surakes_anthem1_modifier
			duration = -1
		}
	}
}

#Anthem: The Legacy of Karqaslu
country_event = {
	id = surakes_missions.18
	title = surakes_missions.18.t
	desc = surakes_missions.18.d
	picture = STREET_SPEECH_eventPicture

	is_triggered_only = yes

	option = {
	name = surakes_missions.17.a
		add_country_modifier = {
			name =  surakes_anthem2_modifier
			duration = -1
		}
	}
}

#Anthem: The Trade Networks of Bulwar	
country_event = {
	id = surakes_missions.19
	title = surakes_missions.19.t
	desc = surakes_missions.19.d
	picture = STREET_SPEECH_eventPicture

	is_triggered_only = yes

	option = {
	name = surakes_missions.17.a
		add_country_modifier = {
			name =  surakes_anthem3_modifier
			duration = -1
		}
	}
}

#Anthem: The Beauty of our Gardens
country_event = {
	id = surakes_missions.20
	title = surakes_missions.20.t
	desc = surakes_missions.20.d
	picture = STREET_SPEECH_eventPicture

	is_triggered_only = yes

	option = {
	name = surakes_missions.17.a
		add_country_modifier = {
			name =  surakes_anthem4_modifier
			duration = -1
		}
	}
}

#Anthem: The Gifts of Brasan the Emancipator
country_event = {
	id = surakes_missions.21
	title = surakes_missions.21.t
	desc = surakes_missions.21.d
	picture = STREET_SPEECH_eventPicture

	is_triggered_only = yes

	option = {
	name = surakes_missions.17.a
		add_country_modifier = {
			name =  surakes_anthem5_modifier
			duration = -1
		}
	}
}

#Anthem: The Fight for our Independence
country_event = {
	id = surakes_missions.22
	title = surakes_missions.22.t
	desc = surakes_missions.22.d
	picture = STREET_SPEECH_eventPicture

	is_triggered_only = yes

	option = {
	name = surakes_missions.17.a
		add_country_modifier = {
			name =  surakes_anthem6_modifier
			duration = -1
		}
	}
}

#Anthem: The Deeds of Jaher
country_event = {
	id = surakes_missions.23
	title = surakes_missions.23.t
	desc = surakes_missions.23.d
	picture = STREET_SPEECH_eventPicture

	is_triggered_only = yes

	option = {
	name = surakes_missions.17.a
		add_country_modifier = {
			name =  surakes_anthem7_modifier
			duration = -1
		}
	}
}

#Anthem: The Revelations of Jaddar
country_event = {
	id = surakes_missions.24
	title = surakes_missions.24.t
	desc = surakes_missions.24.d
	picture = STREET_SPEECH_eventPicture

	is_triggered_only = yes

	option = {
	name = surakes_missions.17.a
		add_country_modifier = {
			name =  surakes_anthem8_modifier
			duration = -1
		}
	}
}

#Lessons from Our Past - Reward Eduz-Vacyn
country_event = {
	id = surakes_missions.25
	title = surakes_missions.25.t
	desc = surakes_missions.25.d
	picture = BIG_BOOK_eventPicture

	is_triggered_only = yes
	
	option = {
		name = surakes_missions.15.a
		random_list = {
			15 = {
				631 = { change_trade_goods = damestear }
			}
			15 = {
				631 = { change_trade_goods = precursor_relics }
			}
			50 = {
				add_treasury = 1500
				add_prestige = 25
			}
			20 = {
				add_country_modifier = {
					name = surakes_lessons3_modifier #Deposit of Genie Lamps
					duration = -1
				}
			}
		}
	}
}

#Lessons from Our Past - Reward Bulwar
country_event = {
	id = surakes_missions.26
	title = surakes_missions.26.t
	desc = surakes_missions.26.d
	picture = BIG_BOOK_eventPicture

	is_triggered_only = yes
	
	option = {
		name = surakes_missions.15.a
		random_list = {
			15 = {
				601 = { change_trade_goods = damestear }
			}
			15 = {
				601 = { change_trade_goods = precursor_relics }
			}
			50 = {
				add_treasury = 1500
				add_prestige = 25
			}
			20 = {
				add_country_modifier = {
					name = surakes_lessons1_modifier #Crown of the God-Kings of Bulwar
					duration = -1
				}
			}
		}
	}
}

#Lessons from Our Past - Reward Karqaslu
country_event = {
	id = surakes_missions.27
	title = surakes_missions.27.t
	desc = surakes_missions.27.d
	picture = BIG_BOOK_eventPicture

	is_triggered_only = yes
	
	option = {
		name = surakes_missions.15.a
		random_list = {
			15 = {
				591 = { change_trade_goods = damestear }
			}
			15 = {
				591 = { change_trade_goods = precursor_relics }
			}
			50 = {
				add_treasury = 1500
				add_prestige = 25
			}
			20 = {
				add_country_modifier = {
					name = surakes_lessons2_modifier #Sword of the Last King of Karqaslu
					duration = -1
				}
			}
		}
	}
}