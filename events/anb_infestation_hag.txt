namespace = infestation_hag

# for testing, use event infestation_hag.0 to force spawn in one of your provinces

# notes on hags:
# hag infestations have three sizes, 1, 2, 3 with three being the largest
# hags can be civilized as racial minority
# hags can rebel at size 2 or 3, with demands to civilize

# possible flags and province modifiers
# infestation_present - province flag, universal
# infestation_hag_1, infestation_hag_2, infestation_hag_3 for sizes of hags
# infestation_hag_rebels - country flag so that we can clean up after a rebellion

# spawn hag infestation in this province and start event loop
province_event = {
    id = infestation_hag.0
    title = infestation_hag.0.t
    desc = infestation_hag.0.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = yes
    is_triggered_only = yes
    
    immediate = {
        hidden_effect = {
            set_province_flag = infestation_present # important; or event loop will exit
        }
    }
    
    option = {
        ai_chance = { factor = 100 }
        add_permanent_province_modifier  = {
            name = infestation_hag_1
            duration = -1  
            desc = infestation_hag_1_tooltip
        }
        province_event = { id = infestation_hag.100 days = 1 } # spawn response event
    }
    after = {
        # first event in half a year, ish; start event loop
        #province_event = { id = infestation_hag.1 days = 100 random = 100 }
        set_province_flag = infestation_pulse_flag #we don't want to start pulsing yet
        add_province_triggered_modifier = infestation_hag_monthly_pulse # we don't want this to start pulsing yet
    }
}

# random event dispatcher, on a loop until infestation_present is removed
province_event = {
    id = infestation_hag.1
    title = infestation_hag.1.t
    desc = infestation_hag.1.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = yes
    is_triggered_only = yes
    

    trigger = {
        has_province_flag = infestation_present
        OR = { 
            has_province_modifier = infestation_hag_1
            has_province_modifier = infestation_hag_2
            has_province_modifier = infestation_hag_3
        }
    }
	
	option = {
		hidden_effect = {
			random_list = {
				1 = { # vanish
					trigger = { has_province_modifier = infestation_hag_1 }
					province_event = { id = infestation_hag.101 }
				}
				4 = {
					trigger = {
						owner = { num_of_cities = 2 } # you have something to migrate to
						any_neighbor_province = { 
							owned_by = ROOT
							NOT = { has_province_flag = infestation_present }
						}
					}
					province_event = { id = infestation_hag.110 }
				}
				1 = { # migrate across border
					trigger = {
						any_neighbor_province = { 
							NOT = { owned_by = ROOT }
							NOT = { has_province_flag = infestation_present }
						}
					}
					province_event = { id = infestation_hag.111 }
				}
				1 = { # banish (migrate to random neighbour)
					modifier = {
						factor = 3 # more likely if generous quest rewards/adverturer nation
						OR = {
							owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
							owner = { has_adventurer_reform = yes }
						}
					}
					province_event = { id = infestation_hag.115 }
				}
				4 = { # Spreads internally
					trigger = { 
						OR = { 
							has_province_modifier = infestation_hag_2 
							has_province_modifier = infestation_hag_3
						}
					}
					province_event = { id = infestation_hag.120 }
				}
				2 = { # Spreads across borders
					trigger = { 
						OR = { 
							has_province_modifier = infestation_hag_2 
							has_province_modifier = infestation_hag_3
						}
					}
					province_event = { id = infestation_hag.121 }
				}
				4 = { # grows
					trigger = { 
						OR = { 
							has_province_modifier = infestation_hag_1 
							has_province_modifier = infestation_hag_2
						}
					}
					province_event = { id = infestation_hag.130 }
				}
				1 = { # shrinks
					trigger = { 
						OR = { 
							has_province_modifier = infestation_hag_2 
							has_province_modifier = infestation_hag_3
						}
					}
					province_event = { id = infestation_hag.131 }
				}
				6 = { # devastation
					province_event = { id = infestation_hag.140 }
				}
				1 = { # provoke rebellion
					trigger = { 
						OR = { 
							has_province_modifier = infestation_hag_2 
							has_province_modifier = infestation_hag_3
						}
					}
					modifier = {
						factor = 5 # more likely if generous quest rewards/adverturer nation
						OR = {
							owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
							owner = { has_adventurer_reform = yes }
						}
					}
					province_event = { id = infestation_hag.146 }
				}
				1 = { # infestation killed -> shrinks/vanishes
					modifier = {
						factor = 5 # more likely if generous quest rewards/adverturer nation
						OR = {
							owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
							owner = { has_adventurer_reform = yes }
						}
					}
					province_event = { id = infestation_hag.150 }
				}
			}
		}
	}
    
    #after = {
    #    # loop this random event dispatcher
    #    # about a year between events
    #    #province_event = { id = infestation_hag.1 days = 300 random = 100 }
    #}
}


# notification of infestation spawning
province_event = {
    id = infestation_hag.100
    title = infestation_hag.100.t
    desc = infestation_hag.100.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    option = {
        # what's the worst that can happen?
        name = infestation_hag.100.a
        ai_chance = { factor = 50 }
    }
    option = {
        # grant adventurer's generous rewards to look after it
        name = infestation_hag.100.b
        ai_chance = { 
            factor = 20 
            modifier = {
                factor = 0
                owner = { is_in_deficit = yes }
            }
        }
        trigger = {
            owner = { has_estate = estate_adventurers }
            owner = { NOT = { has_estate_privilege = estate_adventurers_generous_quest_rewards } }
        }
        owner = { set_estate_privilege = estate_adventurers_generous_quest_rewards }
    }
    option = {
        # the adventurer's are on it
        name = infestation_hag.100.c
        ai_chance = { factor = 50 }
        trigger = {
            owner = { has_estate = estate_adventurers }
            owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
        }
    }
    option = {
        # we are adventurers, deal with it ourselves!
        name = infestation_hag.100.e
        ai_chance = { factor = 50 }
        trigger = {
            owner = { has_adventurer_reform = yes }
        }
    }
}

# infestation vanishes on its own
province_event = {
    id = infestation_hag.101
    title = infestation_hag.101.t
    desc = infestation_hag.101.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
        has_province_modifier = infestation_hag_1
    }
    
    option = {
        # oh, thank goodness
        name = infestation_hag.101.a
        ai_chance = { factor = 100 }
        remove_province_modifier = infestation_hag_1
        hidden_effect = { remove_province_triggered_modifier = infestation_hag_monthly_pulse }
        cleanup_infestation = yes
    }
}

# infestation migrates to adjacent province internally event
province_event = {
    id = infestation_hag.110
    title = infestation_hag.110.t
    desc = infestation_hag.110.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = yes
    is_triggered_only = yes
    
    trigger = {
        owner = { num_of_cities = 2 } # you have something to migrate to
        any_neighbor_province = { 
            owned_by = ROOT
            NOT = { has_province_flag = infestation_present }
            has_influencing_fort = no # forts prevent infestations from spawning if active
        }
    }
    
    immediate = {
        hidden_effect = {
            random_neighbor_province = {
                limit = {
                    owned_by = ROOT
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
                save_event_target_as = infestation_migration_target
            }
            event_target:infestation_migration_target = {
                set_province_flag = infestation_present
            }
            hidden_effect = { remove_province_triggered_modifier = infestation_hag_monthly_pulse }
            cleanup_infestation = yes
        }
    }
    option = {
        name = infestation_hag.110.a
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_hag_3
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_hag_3
                duration = -1  
                desc = infestation_hag_3_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_hag_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_hag_3
    }    
    option = {
        name = infestation_hag.110.b
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_hag_2
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_hag_2
                duration = -1  
                desc = infestation_hag_2_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_hag_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_hag_2
    }    
    option = {
        name = infestation_hag.110.c
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_hag_1
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_hag_1
                duration = -1  
                desc = infestation_hag_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_hag_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_hag_1
    }    
    
    #after = {
    #    # first event in half a year, ish
    #    event_target:infestation_migration_target = {
    #        province_event = { id = infestation_hag.1 days = 100 random = 100 }
    #    }
    #}
}

# infestation migrates across border to adjacent province event
province_event = {
    id = infestation_hag.111
    title = infestation_hag.111.t
    desc = infestation_hag.111.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        any_neighbor_province = { 
            NOT = { owned_by = ROOT }
            NOT = { has_province_flag = infestation_present }
            has_influencing_fort = no # forts prevent infestations from spawning if active
        }
    }
    
    immediate = {
        hidden_effect = {
            random_neighbor_province = {
                limit = {
                    NOT = { owned_by = ROOT }
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
                save_event_target_as = infestation_migration_target
            }
            event_target:infestation_migration_target = {
                set_province_flag = infestation_present
            }
            hidden_effect = { remove_province_triggered_modifier = infestation_hag_monthly_pulse }
            cleanup_infestation = yes
        }
    }
    
    option = {
        name = infestation_hag.111.a
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_hag_3
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_hag_3
                duration = -1  
                desc = infestation_hag_3_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_hag_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_hag_3
    }
    option = {
        name = infestation_hag.111.b
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_hag_2
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_hag_2
                duration = -1  
                desc = infestation_hag_2_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_hag_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_hag_2
    }
    option = {
        name = infestation_hag.111.c
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_hag_1
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_hag_1
                duration = -1  
                desc = infestation_hag_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_hag_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_hag_1
    }
    after = {
        event_target:infestation_migration_target = {
            province_event = { id = infestation_hag.112 }
        }
    }
}

# infestation migrated/spread across border into this province event
# basically the same as infestation_hag.100, but with opinion modifier
# and more flavourful descriptions
province_event = {
    id = infestation_hag.112
    title = infestation_hag.112.t
    desc = infestation_hag.112.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    immediate = {
    }
    option = {
        # How could they do this to us!?
        name = infestation_hag.112.a
        ai_chance = { factor = 50 }
        owner = {
            add_opinion = {
                who = FROM
                modifier = infestation_crossed_border
            }
        }
    }
    option = {
        # grant adventurer's generous rewards to look after it
        name = infestation_hag.112.b
        ai_chance = { 
            factor = 20 
            modifier = {
                factor = 0
                owner = { is_in_deficit = yes }
            }
        }
        trigger = {
            owner = { has_estate = estate_adventurers }
            owner = { NOT = { has_estate_privilege = estate_adventurers_generous_quest_rewards } }
        }
        owner = { set_estate_privilege = estate_adventurers_generous_quest_rewards }
        owner = {
            add_opinion = {
                who = FROM
                modifier = infestation_crossed_border
            }
        }
    }
    option = {
        # the adventurer's are on it
        name = infestation_hag.112.c
        ai_chance = { factor = 50 }
        trigger = {
            owner = { has_estate = estate_adventurers }
            owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
        }
        owner = {
            add_opinion = {
                who = FROM
                modifier = infestation_crossed_border
            }
        }
    }
    option = {
        # we are adventurers, deal with it ourselves!
        name = infestation_hag.112.e
        ai_chance = { factor = 50 }
        trigger = {
            owner = { has_adventurer_reform = yes }
        }
        owner = {
            add_opinion = {
                who = FROM
                modifier = infestation_crossed_border
            }
        }
    }
    
    #after = {
    #    # first event in half a year, ish
    #    province_event = { id = infestation_hag.1 days = 100 random = 100 }
    #}
}

# banished (migrate to a neighbour at random)
province_event = {
    id = infestation_hag.115
    title = infestation_hag.115.t
    desc = infestation_hag.115.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
        
    trigger = {
        owner = {
            any_neighbor_country = {
                any_owned_province = {
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
            }
        }
    }
    
    immediate = {
        hidden_effect = {
            owner = {
                random_neighbor_country = {
                    limit = {
                        any_owned_province = {
                            NOT = { has_province_flag = infestation_present }
                            has_influencing_fort = no # active forts prevent infestations from spawning 
                        }
                    }
                    save_event_target_as = infestation_migration_country_target
                }
            }
            event_target:infestation_migration_country_target = {
                random_owned_province = {
                    limit = { 
                        NOT = { has_province_flag = infestation_present } 
                        has_influencing_fort = no # forts prevent infestations from spawning if active
                    }
                    save_event_target_as = infestation_migration_target
                }
            }
            event_target:infestation_migration_target = {
                set_province_flag = infestation_present
            }
            hidden_effect = { remove_province_triggered_modifier = infestation_hag_monthly_pulse }
            cleanup_infestation = yes
        }
    }
    
    option = {
        name = infestation_hag.115.a
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_hag_3
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_hag_3
                duration = -1  
                desc = infestation_hag_3_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_hag_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_hag_3
    }
    option = {
        name = infestation_hag.115.b
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_hag_2
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_hag_2
                duration = -1  
                desc = infestation_hag_2_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_hag_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_hag_2
    }
    option = {
        name = infestation_hag.115.c
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_hag_1
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_hag_1
                duration = -1  
                desc = infestation_hag_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_hag_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_hag_1
    }
    after = {
        event_target:infestation_migration_target = {
            province_event = { id = infestation_hag.112 }
        }
        if = {
            limit = { owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards } }
            owner = {
                add_estate_influence_modifier = {
                    estate = estate_adventurers
                    desc = handled_infestation
                    duration = 3650
                    influence = 5
                }
            }
        }
    }
}

# infestation spreads to adjacent province internally event
province_event = {
    id = infestation_hag.120
    title = infestation_hag.120.t
    desc = infestation_hag.120.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        owner = { num_of_cities = 2 } # you have something to migrate to
        any_neighbor_province = { 
            owned_by = ROOT
            NOT = { has_province_flag = infestation_present }
            has_influencing_fort = no # forts prevent infestations from spawning if active
        }
        OR = {
            has_province_modifier = infestation_hag_3
            has_province_modifier = infestation_hag_2
        }
    }
    immediate = {
        hidden_effect = {
            random_neighbor_province = {
                limit = {
                    owned_by = ROOT
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
                save_event_target_as = infestation_spread_target
            }
            event_target:infestation_spread_target = {
                set_province_flag = infestation_present
            }
        }
    }
    option = {
        name = infestation_hag.120.a
        ai_chance = { factor = 100 }
        goto = infestation_spread_target
        event_target:infestation_spread_target = {
            add_permanent_province_modifier  = {
                name = infestation_hag_1
                duration = -1  
                desc = infestation_hag_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_hag_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
    }
    #after = {
    #    # first event in half a year, ish
    #    event_target:infestation_spread_target = {
    #        province_event = { id = infestation_hag.1 days = 100 random = 100 }
    #    }
    #}
}

# infestation spreads across border to adjacent province event
province_event = {
    id = infestation_hag.121
    title = infestation_hag.121.t
    desc = infestation_hag.121.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        any_neighbor_province = { 
            NOT = { owned_by = ROOT }
            NOT = { has_province_flag = infestation_present }
            has_influencing_fort = no # forts prevent infestations from spawning if active
        }
        OR = {
            has_province_modifier = infestation_hag_3
            has_province_modifier = infestation_hag_2
        }
    }
    immediate = {
        hidden_effect = {
            random_neighbor_province = {
                limit = {
                    NOT = { owned_by = ROOT }
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
                save_event_target_as = infestation_migration_target
            }
            event_target:infestation_migration_target = {
                set_province_flag = infestation_present
            }
        }
    }
    option = {
        name = infestation_hag.121.a
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_hag_1
                duration = -1  
                desc = infestation_hag_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_hag_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
    }
    after = {
        event_target:infestation_migration_target = {
            province_event = { id = infestation_hag.112 }
        }
    }
}

# infestation grows in strength
province_event = {
    id = infestation_hag.130
    title = infestation_hag.130.t
    desc = infestation_hag.130.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
        OR = { 
            has_province_modifier = infestation_hag_1 
            has_province_modifier = infestation_hag_2
        }
    }
    option = {
        # should we be worried?
        name = infestation_hag.130.a
        ai_chance = { factor = 100 }
        if = {
            limit = { has_province_modifier = infestation_hag_2 }
            hidden_effect = { remove_province_modifier = infestation_hag_2 }
            add_permanent_province_modifier  = {
                name = infestation_hag_3
                duration = -1  
                desc = infestation_hag_3_tooltip
            }
        }
        if = {
            limit = { has_province_modifier = infestation_hag_1 }
            hidden_effect = { remove_province_modifier = infestation_hag_1 }
            add_permanent_province_modifier  = {
                name = infestation_hag_2
                duration = -1  
                desc = infestation_hag_2_tooltip
            }
        }
    }
}

# infestation shrinks in strength
province_event = {
    id = infestation_hag.131
    title = infestation_hag.131.t
    desc = infestation_hag.131.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
        OR = { 
            has_province_modifier = infestation_hag_2 
            has_province_modifier = infestation_hag_3
        }
    }
    
    option = {
        # should we be worried?
        name = infestation_hag.131.a
        ai_chance = { factor = 100 }
        if = {
            limit = { has_province_modifier = infestation_hag_2 }
            hidden_effect = { remove_province_modifier = infestation_hag_2 }
            add_permanent_province_modifier  = {
                name = infestation_hag_1
                duration = -1  
                desc = infestation_hag_1_tooltip
            }
        }
        if = {
            limit = { has_province_modifier = infestation_hag_3 }
            hidden_effect = { remove_province_modifier = infestation_hag_3 }
            add_permanent_province_modifier  = {
                name = infestation_hag_2
                duration = -1  
                desc = infestation_hag_2_tooltip
            }
        }
    }
}

# devastation
province_event = {
    id = infestation_hag.140
    title = infestation_hag.140.t
    desc = infestation_hag.140.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    option = {
        name = infestation_hag.140.a
        ai_chance = { factor = 20 }
        add_devastation = 5
    }
    option = {
        # crack down hard
        name = infestation_hag.140.b
        trigger = { 
            OR = { 
                has_province_modifier = infestation_hag_2 
                has_province_modifier = infestation_hag_3
            }
        }
        ai_chance = { factor = 80 }
        add_devastation = 10
        province_event = { id = infestation_hag.131 days = 30 } # shrinks in size
    }
    option = {
        # wipe them out
        name = infestation_hag.140.c
        trigger = { 
            has_province_modifier = infestation_hag_1 
        }
        ai_chance = { factor = 80 }
        add_devastation = 15
        province_event = { id = infestation_hag.101 days = 30 } # vanishes
    }
}

# adventurers provoke rebellion
province_event = {
    id = infestation_hag.146
    title = infestation_hag.146.t
    desc = infestation_hag.146.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
        
    option = {
        # provoke them!
        name = infestation_hag.146.a
        ai_chance = { factor = 50 }
        set_country_flag = infestation_hag_rebels
        spawn_rebels = {
            type = infestation_hag_rebels
            size = 1
        }
    }
    option = {
        # a terrible idea
        name = infestation_hag.146.b
        ai_chance = { factor = 50 }
    }
}

# post rebellion cleanup - hags removed from country
country_event = {
    id = infestation_hag.148
    title = infestation_hag.148.t
    desc = infestation_hag.148.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_country_flag = infestation_hag_rebels
        FROM = {
            tag = REB
        }
        NOT = { has_spawned_rebels = infestation_hag_rebels }
    }
    option = {
        # hags utterly defeated
        name = infestation_hag.148.a
        clr_country_flag = infestation_hag_rebels
        hidden_effect = {
            every_owned_province = {
                limit = {
                    OR = {
                        has_province_modifier = infestation_hag_1
                        has_province_modifier = infestation_hag_2
                        has_province_modifier = infestation_hag_3
                    }
                }
                remove_province_modifier = infestation_hag_1
                remove_province_modifier = infestation_hag_2
                remove_province_modifier = infestation_hag_3
                hidden_effect = { remove_province_triggered_modifier = infestation_hag_monthly_pulse }
                cleanup_infestation = yes
            }
        }
    }
}

# kill hags
province_event = {
    id = infestation_hag.150
    title = infestation_hag.150.t
    desc = infestation_hag.150.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
    }
    
    option = {
        # great job!
        name = infestation_hag.150.a
        ai_chance = { factor = 100 }
        if = {
			limit = { 
				has_province_modifier = infestation_hag_1 
			}
			remove_province_modifier = infestation_hag_1 
			hidden_effect = { remove_province_triggered_modifier = infestation_hag_monthly_pulse }
			cleanup_infestation = yes
		}
		else_if = {
			limit = { 
				has_province_modifier = infestation_hag_2 
			}
			remove_province_modifier = infestation_hag_2
			add_permanent_province_modifier  = { 
				name = infestation_hag_1
				duration = -1
			}
		}
		else_if = {
			limit = { 
				has_province_modifier = infestation_hag_3 
			}
			remove_province_modifier = infestation_hag_3 
			add_permanent_province_modifier  = { 
				name = infestation_hag_2
				duration = -1
			}
		}
        owner = {
            add_estate_influence_modifier = {
                estate = estate_adventurers
                desc = handled_infestation
                duration = 3650
                influence = 5
            }
        }
    }
}

# infestation successfully negotiated with; some upside with minor downside
province_event = {
    id = infestation_hag.155
    title = infestation_hag.155.t
    desc = infestation_hag.155.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
	
	option = {
		#negotiations
		name = infestation_hag.155.a
		ai_chance = { factor = 50 }
		if = {
			limit = { 
				has_province_modifier = infestation_hag_1 
			}
			remove_province_modifier = infestation_hag_1
			hidden_effect = { remove_province_triggered_modifier = infestation_hag_monthly_pulse }
			cleanup_infestation = yes
			add_permanent_province_modifier  = { 
				name = infestation_hag_coven
				duration = 1825
			}
		}
		else_if = {
			limit = { 
				has_province_modifier = infestation_hag_2 
			}
			remove_province_modifier = infestation_hag_2
			hidden_effect = { remove_province_triggered_modifier = infestation_hag_monthly_pulse }
			cleanup_infestation = yes
			add_permanent_province_modifier  = { 
				name = infestation_hag_coven
				duration = 3650
			}
		}
		else_if = {
			limit = { 
				has_province_modifier = infestation_hag_3 
			}
			remove_province_modifier = infestation_hag_3
			hidden_effect = { remove_province_triggered_modifier = infestation_hag_monthly_pulse }
			cleanup_infestation = yes
			add_permanent_province_modifier  = { 
				name = infestation_hag_coven
				duration = 7300
			}
		}
	}
	
	option = {
		#end negotiations
		name = infestation_hag.155.b
		ai_chance = { factor = 50 }
		set_country_flag = infestation_hag_rebels
        spawn_rebels = {
            type = infestation_hag_rebels
            size = 1
        }
	}
}

# cleanup event; wipes all trace of this infestation from the province
# this is useful for debugging
province_event = {
    id = infestation_hag.2000
    title = infestation_hag.2000.t
    desc = infestation_hag.2000.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = yes
    is_triggered_only = yes
    
    option = {
        # cleanup; hidden
        name = infestation_hag.2000.a
        ai_chance = { factor = 100 }
        remove_province_modifier = infestation_hag_1
        remove_province_modifier = infestation_hag_2
        remove_province_modifier = infestation_hag_3
        hidden_effect = { remove_province_triggered_modifier = infestation_hag_monthly_pulse }
        cleanup_infestation = yes
    }
}