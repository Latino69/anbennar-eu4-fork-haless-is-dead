
#Special: Aldresian Knights (Emperor or Orda Aldresia only)
merc_aldresian_knights = {
    regiments_per_development = 0.01
	cavalry_weight = 0.4
	cavalry_cap = 6
	home_province = 423
	sprites = { westerngfx_sprite_pack }
    trigger = {
		NOT = { has_country_modifier = monstrous_nation}
		NOT = { has_country_modifier = centaur_military }
		OR = {
			is_emperor = yes
			tag = A77
			tag = Z01
		}
	}
	cost_modifier = 0.8
	modifier = {
		shock_damage_received = -0.1
		land_morale = 0.1
	}
}

merc_battlemages = {
    regiments_per_development = 0.02
    cavalry_weight = 0
    artillery_weight = 0
    cavalry_cap = 0
    cost_modifier = 0.75
	trigger = {
		has_estate_privilege = estate_mages_battlemage_academies
		NOT = { has_country_modifier = centaur_military }
	}
    modifier = {
		infantry_shock = 2
		land_morale = 0.1
		reinforce_speed = -0.5
    }
    # No home province means local mercenary company
}


# Onyx Legion - special religious army for Nerat
merc_onyx_legion = {
    regiments_per_development = 0.02
	cavalry_weight = 0.1
	artillery_weight = 0
	cavalry_cap = 2
	sprites = { westerngfx_sprite_pack }
    trigger = {
		NOT = { has_country_modifier = monstrous_nation}
		NOT = { has_country_modifier = centaur_military }
		OR = {
			capital_scope = { 
				OR = {
					continent = europe
				}
			}
			has_country_modifier = mercenaries_to_the_standard
			has_country_modifier = glut_of_mercs
		}
		has_personal_deity = nerat
		NOT = { has_global_flag = remove_merc_onyx_legion }
	}
	cost_modifier = 0.75
	modifier = {
		discipline = 0.1
		reinforce_speed = -0.5
	}
}

#Patrician Guard (Escann) - elite warriors for the Castonath patrician
merc_patrician_guard = {
    regiments_per_development = 0.02
	artillery_weight = 0.1
	home_province = 833
	sprites = { westerngfx_sprite_pack }
    trigger = {
		has_estate = estate_castonath_patricians
		NOT = { has_country_modifier = monstrous_nation}
		NOT = { has_country_modifier = centaur_military }
		castonath_area = { owned_by = ROOT }
	}
	cost_modifier = 0.9
	modifier = {
		discipline = 0.1
	}
}


# Var Kultoz
merc_rezankand_var_kultoz = {
    regiments_per_development = 0.05
    home_province = 2333 # Kaio'Oron
	cavalry_weight = 0.2
    artillery_weight = 0.4
	cavalry_cap = 4
	sprites = { dlc056_mlo_sprite_pack sav_base_sprite_pack westerngfx_sprite_pack }
    trigger = {
		has_country_flag = rezankand_elite_division
	}
	cost_modifier = 1.25
	modifier = { 
		reinforce_speed = -0.5
		movement_speed = 0.1
		discipline = 0.1
	}
}


# Amldahvad Guard
merc_amldahvad_guard = {
    regiments_per_development = 0.05
    home_province = 4122 # Verkal Vazkron
	cavalry_weight = 0
    artillery_weight = 0.5
	cavalry_cap = 0
	sprites = { westerngfx_sprite_pack }
    trigger = {
		has_country_flag = amldahvad_guard_founded
	}
	cost_modifier = 0.8
	modifier = { 
		recover_army_morale_speed = 0.20
		discipline = 0.1
	}
}

# Domandrod Lightbringers
merc_domandrod_lightbringers = {
    regiments_per_development = 0.03
	cavalry_weight = 0.1
	cavalry_cap = 2
	sprites = { sav_base_sprite_pack westerngfx_sprite_pack }
    trigger = {
		tag = H21 #Eordand
	}
	cost_modifier = 1
	modifier = { 
		discipline = 0.05
	}
}


# The Golden Shield Company
merc_verkalgulan_the_golden_shield_company = {
    regiments_per_development = 0.02
    home_province = 2914 # Verkal Gulan
	cavalry_weight = 0
    artillery_weight = 0.5
	cavalry_cap = 0
	sprites = { westerngfx_sprite_pack }
    trigger = {
		OR = {
			has_country_modifier = verkalgulan_the_golden_shield_company
			any_subject_country = { has_country_modifier = verkalgulan_the_golden_shield_company }
		}
	}
	cost_modifier = 0.75
	modifier = {
		infantry_power = 0.2
		shock_damage_received = -0.1
		reinforce_speed = -0.5
	}
}

# The Citadel Cannonneers
merc_verkalgulan_the_citadel_cannonneers = {
    regiments_per_development = 0.02
    home_province = 2914 # Verkal Gulan
	cavalry_weight = 0
    artillery_weight = 0.5
	cavalry_cap = 0
	sprites = { westerngfx_sprite_pack }
    trigger = {
		OR = {
			has_country_modifier = verkalgulan_the_golden_shield_company
			any_subject_country = { has_country_modifier = verkalgulan_the_golden_shield_company }
		}
	}
	cost_modifier = 1
	modifier = {
		backrow_artillery_damage = 0.2
		fire_damage = 0.1
		reinforce_speed = -0.5
	}
}

# The King's Gate Banners
merc_verkalgulan_the_kings_gate_banners = {
    regiments_per_development = 0.02
    home_province = 2914 # Verkal Gulan
	cavalry_weight = 0
    artillery_weight = 0.5
	cavalry_cap = 0
	sprites = { westerngfx_sprite_pack }
    trigger = {
		OR = {
			has_country_modifier = verkalgulan_the_golden_shield_company
			any_subject_country = { has_country_modifier = verkalgulan_the_golden_shield_company }
		}
	}
	cost_modifier = 0.6
	modifier = {
		land_morale = 0.1
		reinforce_speed = -0.5
	}
}

# Iron Company
merc_iron_company = {
    regiments_per_development = 0.075
    home_province = 740 # Bal Vroren (Ur Sarmozd)
	cavalry_weight = 0.1
	cavalry_cap = 4
    artillery_weight = 0.1
    trigger = {
		tag = Z50 #Grombar
		has_country_flag = grombar_iron_company_created
	}
	cost_modifier = 0.75
	modifier = { 
		shock_damage_received = -0.1
		fire_damage_received = -0.1
		shock_damage = 0.1
		land_morale = 0.1
	}
}

# Sarisung City Guards
merc_sarisung_city_guard = {
    regiments_per_development = 0.025
	cavalry_weight = 0
	artillery_weight = 0
    trigger = {
		has_country_flag = sarisung_city_guard_mission_flag
	}
	cost_modifier = 1.25
	modifier = { 
		discipline = 0.1
	}
}

# Flamemarked Gnoll Mercenaries (Sugamber mission tree)
merc_sugamber_flamemarked_gnoll = {
	regiments_per_development = 0.025
	cavalry_weight = 0
	artillery_weight = 0
	home_province = 443
	sprites = { Gnoll4_sprite_pack }
    trigger = {
		has_country_flag = sugamber_flamemarked_mercs_flag

	}
	cost_modifier = 0.70 #They take and sell slaves instead of your gold
	modifier = {
		infantry_power = 0.2 #Successors of the Third Xhaz
		movement_speed = 0.2 #Used to moving quickly through marshy Folly
		land_morale = -0.1 #Quick to break if no superiority
		discipline = -0.025 #Unruly Gnolls
        may_recruit_female_generals = yes
        female_advisor_chance = 0.5
	}
}

# Hill Gnoll Mercenaries (Sugamber mission tree)
merc_sugamber_hill_gnoll = {
	regiments_per_development = 0.025
	cavalry_weight = 0
	artillery_weight = 0
	home_province = 913
	sprites = { Gnoll3_sprite_pack }
    trigger = {
		has_country_flag = sugamber_hill_mercs_flag
	}
	cost_modifier = 0.70 #They take and sell slaves instead of your gold
	modifier = {
		shock_damage = 0.2 #Adept at ambushes and traps
		land_attrition = -0.2 #Easy palate to please
		land_morale = -0.1 #Quick to break if no superiority
		discipline = -0.025 #Unruly Gnolls
        may_recruit_female_generals = yes
        female_advisor_chance = 0.5
	}
}

# Bloodfield Riders
merc_bloodfield_riders = {
    regiments_per_development = 0.05
    home_province = 96 #Enteben
	cavalry_weight = 0.4
    artillery_weight = 0.2
	cavalry_cap = 20
	sprites = { dlc056_mlo_sprite_pack sav_base_sprite_pack westerngfx_sprite_pack }
    trigger = {
		has_country_flag = rubenaire_bloodfield_riders
	}
	cost_modifier = 0.50
	modifier = { 
		reinforce_speed = -0.25
		fire_damage = 0.1
		movement_speed = 0.1
		cavalry_power = 0.2
	}
}

#Lake Fed
#Joglik Uts
merc_joglik_ut_band = {
    regiments_per_development = 0.05
	cavalry_weight = 0.2
    artillery_weight = 0.4
	cavalry_cap = 4
	home_province = 5284
	sprites = { westerngfx_sprite_pack }
    trigger = {
		NOT = { has_country_modifier = monstrous_nation }
		NOT = { has_country_modifier = centaur_military }
		capital_scope = {  superregion = forbidden_lands_superregion }
		any_known_country = {
			has_country_modifier = centaur_military
			war_with = ROOT
		}
	}
	cost_modifier = 0.5
	modifier = {
		discipline = 0.1
		shock_damage_received = -0.25
	}
}

#Tlaric Gangs
merc_tlaric_gangs = {
    regiments_per_development = 0.1
	cavalry_weight = 0
	cavalry_cap = 0
	home_province = 5304
	sprites = { westerngfx_sprite_pack }
    trigger = {
		NOT = { has_country_modifier = monstrous_nation }
		NOT = { has_country_modifier = centaur_military }
		capital_scope = {  superregion = forbidden_lands_superregion }
		5304 = { has_province_modifier = tlar }
	}
	cost_modifier = 0.5
	modifier = {
		discipline = -0.1
		land_morale = 0.25
	}
}

#Westmarchers
merc_westmarchers = {
    regiments_per_development = 0.07
	cavalry_weight = 0.05
	cavalry_cap = 5
	home_province = 5305
	sprites = { westerngfx_sprite_pack }
    trigger = {
		NOT = { has_country_modifier = monstrous_nation }
		NOT = { has_country_modifier = centaur_military }
		capital_scope = {  superregion = forbidden_lands_superregion }
	}
	cost_modifier = 0.8
	modifier = {
		infantry_power = 0.125
	}
}

#Heartland Guard
merc_heartland_guard = {
    regiments_per_development = 0.06
	cavalry_weight = 0.05
	cavalry_cap = 5
	home_province = 5276
	sprites = { westerngfx_sprite_pack }
    trigger = {
		NOT = { has_country_modifier = monstrous_nation }
		NOT = { has_country_modifier = centaur_military }
		capital_scope = {  superregion = forbidden_lands_superregion }
	}
	cost_modifier = 0.8
	modifier = {
		land_morale = 0.15
	}
}

#Bright Wolves
merc_bright_wolves = {
    regiments_per_development = 0.06
	cavalry_weight = 0.05
	cavalry_cap = 3
	home_province = 5253
	sprites = { westerngfx_sprite_pack }
    trigger = {
		NOT = { has_country_modifier = monstrous_nation }
		NOT = { has_country_modifier = centaur_military }
		capital_scope = {  superregion = forbidden_lands_superregion }
	}
	cost_modifier = 0.8
	modifier = {
		shock_damage = 0.075
		loot_amount = 0.2
	}
}

#Eastmen
merc_eastmen = {
    regiments_per_development = 0.07
	cavalry_weight = 0.05
	cavalry_cap = 4
	home_province = 5226
	sprites = { westerngfx_sprite_pack }
    trigger = {
		NOT = { has_country_modifier = monstrous_nation }
		NOT = { has_country_modifier = centaur_military }
		capital_scope = {  superregion = forbidden_lands_superregion }
	}
	cost_modifier = 1
	modifier = {
		movement_speed = 0.1
		reinforce_speed = 0.25
	}
}

#Amber Band
merc_amber_band = {
    regiments_per_development = 0.04
	cavalry_weight = 0.05
	cavalry_cap = 4
	sprites = { westerngfx_sprite_pack }
    trigger = {
		NOT = { has_country_modifier = monstrous_nation }
		NOT = { has_country_modifier = centaur_military }
		capital_scope = {  superregion = forbidden_lands_superregion }
	}
	cost_modifier = 0.8
	modifier = {
		discipline = 0.025
	}
}

#Hired Bandits
merc_hired_bandits = {
    regiments_per_development = 0.09
	cavalry_weight = 0.09
	cavalry_cap = 6
	sprites = { westerngfx_sprite_pack }
    trigger = {
		NOT = { has_country_modifier = monstrous_nation }
		NOT = { has_country_modifier = centaur_military }
		capital_scope = {  superregion = forbidden_lands_superregion }
	}
	cost_modifier = 0.6
	modifier = {
		land_morale = -0.1
		discipline = -0.025
		land_attrition = -0.1
		loot_amount = 0.15
	}
}

#Viollkuske Battalion
merc_442nd = {
    regiments_per_development = 0.05
	artillery_weight = 0.4
	cavalry_weight = 0
	cavalry_cap = 0
	sprites = { westerngfx_sprite_pack }
    trigger = {
		has_country_flag = go_for_broke
	}
	cost_modifier = 1
	modifier = {
		land_morale = 0.1
		fire_damage = 0.2
	}
}

#Ogrillon Guardsmen
merc_ogrillons = {
    regiments_per_development = 0.05
	artillery_weight = 0.4
	cavalry_weight = 0.3
	cavalry_cap = 4
	home_province = 4123
	sprites = { westerngfx_sprite_pack }
    trigger = {
		has_country_modifier = ogrillons
	}
	cost_modifier = 1
	modifier = {
		discipline = 0.1
		land_morale = 0.075
		reinforce_speed = 0.15
	}
}

# The Suraelic Guard
merc_suraelic_guard = {
    regiments_per_development = 0.01
    home_province = 625 # Jaddanzar (Sareyand)
	cavalry_weight = 0.1
	cavalry_cap = 4
    artillery_weight = 0.4
	sprites = { dlc056_mlo_sprite_pack sav_base_sprite_pack westerngfx_sprite_pack }
    trigger = {
		has_country_flag = jaddari_suraelic_guard_founded_flag
		owns_core_province = 625
	}
	cost_modifier = 1.5
	modifier = { 
		reinforce_speed = -0.5
		land_morale = 0.2
		discipline = 0.1
	}
}

merc_reformed_pinklady_band = {
	regiments_per_development = 0.05
	artillery_weight = 0.4
	cavalry_weight = 0.1
	cavalry_cap = 2
	home_province = 280
	sprites = { westerngfx_sprite_pack }
	trigger = {
		NOT = { has_country_modifier = monstrous_nation }
		NOT = { has_country_modifier = centaur_military }
		has_global_flag = new_pinklady_band
		owns_core_province = 280
	}
	cost_modifier = 1.2
	modifier = {
		shock_damage = 0.2		#ferocious
		shock_damage_received = -0.2		#hardened
	}
}

#railskulker mercs
merc_snotfinger_insult_warriors = {
	regiments_per_development = 0.02
	artillery_weight = 0.4
	cavalry_weight = 0.1
	cavalry_cap = 2
	home_province = 2898
	sprites = { westerngfx_sprite_pack }
	trigger = {
		NOT = { has_country_modifier = centaur_military }
		has_country_modifier = goblin_military
		has_country_modifier = cave_goblin_snotfinger_rs
	}
	cost_modifier = 1.2
	modifier = {
		land_morale = 0.1
		shock_damage_received = -0.1
	}
}

merc_truedagger_dwarf_guns = {
	regiments_per_development = 0.02
	artillery_weight = 2
	cavalry_weight = 0.1
	cavalry_cap = 2
	home_province = 4144
	sprites = { westerngfx_sprite_pack }
	trigger = {
		NOT = { has_country_modifier = centaur_military }
		has_country_modifier = goblin_military
		has_country_modifier = cave_goblin_truedagger_rs
	}
	cost_modifier = 1.2
	modifier = {
		artillery_power = 0.1
		artillery_fire = 0.25
	}
}

merc_thieving_arrow_sharpshooters = {
	regiments_per_development = 0.02
	artillery_weight = 0.4
	cavalry_weight = 0.1
	cavalry_cap = 2
	home_province = 2964
	sprites = { westerngfx_sprite_pack }
	trigger = {
		NOT = { has_country_modifier = centaur_military }
		has_country_modifier = goblin_military
		has_country_modifier = cave_goblin_thieving_arrow_rs
	}
	cost_modifier = 1.2
	modifier = {
		fire_damage = 0.1
		infantry_fire = 0.25
	}
}

merc_spiderwretch_spiderriders = {
	regiments_per_development = 0.02
	artillery_weight = 0
	cavalry_weight = 1
	cavalry_cap = 30
	home_province = 4071
	sprites = { westerngfx_sprite_pack }
	trigger = {
		NOT = { has_country_modifier = centaur_military }
		has_country_modifier = goblin_military
		has_country_modifier = cave_goblin_spiderwretch_rs
	}
	cost_modifier = 1.2
	modifier = {
		cavalry_power = 0.1
		cavalry_fire = 0.5
	}
}

merc_greedy_grin_skirmishers = {
	regiments_per_development = 0.02
	artillery_weight = 0.4
	cavalry_weight = 0.1
	cavalry_cap = 2
	home_province = 4086
	sprites = { westerngfx_sprite_pack }
	trigger = {
		NOT = { has_country_modifier = centaur_military }
		has_country_modifier = goblin_military
		has_country_modifier = cave_goblin_greedy_grin_rs
	}
	cost_modifier = 1.2
	modifier = {
		loot_amount = 0.33
		infantry_power = 0.1
	}
}

merc_mountainshark_mountainsharks = {
	regiments_per_development = 0.02
	artillery_weight = 0.4
	cavalry_weight = 1
	cavalry_cap = 30
	home_province = 4052
	sprites = { westerngfx_sprite_pack }
	trigger = {
		NOT = { has_country_modifier = centaur_military }
		has_country_modifier = goblin_military
		has_country_modifier = cave_goblin_mountainshark_rs
	}
	cost_modifier = 1.2
	modifier = {
		shock_damage = 0.1
		infantry_shock = 0.25
	}
}

#Corvurian Dark Legion, unlocked via "The Dark Legion" mission.
merc_corvurian_dark_legion = {
    regiments_per_development = 0.05
    home_province = 441 # Arca Corvur (Corvuria)
	cavalry_weight = 0.1
	cavalry_cap = 4
    artillery_weight = 0.4
	sprites = { easterngfx_sprite_pack }
    trigger = {
		has_country_flag = corvuria_dark_legion
		owns_core_province = 441
	}
	cost_modifier = 0.75
	modifier = { 
		land_morale = 0.1
		discipline = 0.1
	}
}
#Griffon Knights (Marrhold mission tree)
merc_marrhold_griffon_knight = {
	regiments_per_development = 0.025
	cavalry_weight = 1.0
	artillery_weight = 0
	home_province = 895 #Griffonsgate
	sprites = { easterngfx_sprite_pack }
    trigger = {
		has_country_flag = marrhold_griffon_knights
		owns_core_province = 895

	}
	cost_modifier = 0.60
	modifier = {
		cavalry_power = 0.25 #Elite knights
		movement_speed = 0.2 #Literally fly
		prestige_from_land = 1 #Famous
		}
	}
	
#Steel Griffon (Marrhold mission tree)
merc_marrhold_steel_griffon = {
	regiments_per_development = 0.025
	cavalry_weight = 1.0
	artillery_weight = 0
	cavalry_cap = 20
	home_province = 895 #Griffonsgate
	sprites = { easterngfx_sprite_pack }
    trigger = {
		has_country_flag = marrhold_steel_griffon
		owns_core_province = 895
	}
	cost_modifier = 0.80
	modifier = {
		cavalry_power = 0.35 #Elite knights
		movement_speed = 0.2 #Literally fly
		shock_damage_received = -0.1 #Armour
		shock_damage = 0.1 #Armour 
		}
	}

#Griffon Survey_Corps (Marrhold mission tree)
merc_marrhold_griffon_survey = {
	regiments_per_development = 0.10
	cavalry_weight = 0.3
	artillery_weight = 0
	home_province = 871 #Gulenhyl
	sprites = { easterngfx_sprite_pack }
    trigger = {
		has_country_flag = marrhold_survey_corps
		owns_core_province = 880
	}
	cost_modifier = 0.60
	modifier = {
		cavalry_power = 0.20 #Griffon riders
		movement_speed = 0.3 #Flying scout unit
		land_morale = 0.1 #Scout Corps Morale
		}
	}

#Griffon Sky Lancers (Marrhold mission tree)
merc_marrhold_griffon_sky_lancers = {
	regiments_per_development = 0.05
	cavalry_weight = 1.0
	artillery_weight = 0
	cavalry_cap = 24
	home_province = 895 #Griffonsgate
	sprites = { easterngfx_sprite_pack }
    trigger = {
		has_country_flag = marrhold_griffon_sky_lancers
		owns_core_province = 895
	}
	cost_modifier = 0.80
	modifier = {
		cavalry_power = 0.35 #Griffon riders
		movement_speed = 0.2 #Flying unit
		shock_damage = 0.3 #Air charges
		}
	}
	
#Griffon Grenadiers (Marrhold mission tree)
merc_marrhold_griffon_grenadiers = {
	regiments_per_development = 0.15
	cavalry_weight = 0.3
	artillery_weight = 0.2
	home_province = 871 #Gulenhyl
	sprites = { easterngfx_sprite_pack }
    trigger = {
		has_country_flag = marrhold_griffon_grenadiers
		owns_core_province = 880
	}
	cost_modifier = 0.80
	modifier = {
		cavalry_power = 0.25 #Griffon riders
		infantry_power = 0.10 #Veteran grenadiers
		movement_speed = 0.2 #Flying scout unit
		discipline = 0.05 #Scout Corps Morale
		}
	}

#Griffon Bombardiers (Marrhold mission tree)
merc_marrhold_griffon_bombardiers = {
	regiments_per_development = 0.025
	cavalry_weight = 1.0
	artillery_weight = 0
	cavalry_cap = 24
	home_province = 896 #Marrvale
	sprites = { easterngfx_sprite_pack }
    trigger = {
		has_country_flag = marrhold_bombardier
		owns_core_province = 896
	}
	cost_modifier = 1
	modifier = {
		cavalry_power = 0.35 #Griffon riders
		movement_speed = 0.2 #Flying 
		cavalry_fire = 2 #Close Air Support 
	}
}

merc_wineport_watchers = {
	regiments_per_development = 0.05
	cavalry_weight = 0.1
	artillery_weight = 0
	cavalry_cap = 8
	home_province = 101 # Wineport
	sprites = { easterngfx_sprite_pack }
    trigger = {
		has_country_flag = wineport_mercs_unlocked
		owns_core_province = 101
	}
	cost_modifier = 1.0
	modifier = {
		shock_damage_received = -0.10 # Trained to fight Lorents Cavalry 
	}
}

merc_foxalley_foxes = {
	regiments_per_development = 0.025
	cavalry_weight = 0.1
	artillery_weight = 0
	cavalry_cap = 8
	home_province = 81 # Foxalley
	sprites = { easterngfx_sprite_pack }
    trigger = {
		has_country_flag = wineport_mercs_unlocked
		owns_core_province = 81
	}
	cost_modifier = 1.0
	modifier = { 
		movement_speed_onto_off_boat_modifier = 0.33 # These guys are sailors first and foremost (essentially marines)
	}
}

#Wolves of Crovis (Adshaw MT)
merc_wolves_of_crovis = {
	regiments_per_development = 0.025
	cavalry_weight = 0.2
	artillery_weight = 0.3
	cavalry_cap = 12
	home_province = 731 #Rycastle
	sprites = { easterngfx_sprite_pack }
	trigger = {
		custom_trigger_tooltip = {
			tooltip = woc_founded
			has_country_flag = wolves_of_crovis_formed
		}
		owns_core_province = 731
	}
	cost_modifier = 1.2
	modifier = {
		shock_damage_received = -0.1
		land_morale = 0.1
		reinforce_speed = -0.25
	}
}

#Serpent Knights (Adshaw MT)
merc_serpent_knights = {
	regiments_per_development = 0.025
	cavalry_weight = 0.4
	artillery_weight = 0.1
	cavalry_cap = 24
	home_province = 698 #Serpentback
	sprites = { easterngfx_sprite_pack }
	trigger = {
		custom_trigger_tooltip = {
			tooltip = sk_empowered
			has_country_flag = empowered_serpent_knights
		}
		NOT = {
			OR = {
				current_age = age_of_revolutions
				current_age = age_of_absolutism
			}
		}
	}
	cost_modifier = 1.1
	modifier = {
		cavalry_power = 0.075
		land_attrition = -0.1
		movement_speed = 0.1
	}
}
#Malacnari Cannorian Artillery, unlocked via "The Secret of Gunpowder" mission.
merc_malacnari_cannorian_artillery = {
    regiments_per_development = 0.05
    home_province = 1165 # Bosancovac
	cavalry_weight = 0
	cavalry_cap = 0
    artillery_weight = 1
	sprites = { westerngfx_sprite_pack }
    trigger = {
		has_country_flag = malacnar_foreign_artillery
		owns_core_province = 1165
	}
	cost_modifier = 0.4
	modifier = { 
		artillery_power = 0.2
	}
}

#Aelnar Sicrheior Grand Army
merc_aelnar_first_corps = {
    regiments_per_development = 0.1
	cavalry_weight = 0
	cavalry_cap = 0
    artillery_weight = 0.5
	sprites = { westerngfx_sprite_pack }
    trigger = {
		has_country_flag = aelnar_grand_army_1
	}
	cost_modifier = 0.33
	modifier = { 
		fire_damage_received = -0.2
		artillery_power = 0.2
		infantry_power = 0.2
	}
}

merc_aelnar_second_corps = {
    regiments_per_development = 0.1
	cavalry_weight = 0
	cavalry_cap = 0
    artillery_weight = 0.5
	sprites = { westerngfx_sprite_pack }
    trigger = {
		has_country_flag = aelnar_grand_army_2
	}
	cost_modifier = 0.33
	modifier = { 
		fire_damage_received = -0.2
		artillery_power = 0.2
		fire_damage = 0.33
	}
}

merc_aelnar_third_corps = {
    regiments_per_development = 0.1
	cavalry_weight = 0
	cavalry_cap = 0
    artillery_weight = 0.5
	sprites = { westerngfx_sprite_pack }
    trigger = {
		has_country_flag = aelnar_grand_army_3
	}
	cost_modifier = 0.33
	modifier = { 
		fire_damage_received = -0.2
		artillery_power = 0.2
		shock_damage_received = -0.33
		shock_damage = 0.33
	}
}

merc_aelnar_fourth_corps = {
    regiments_per_development = 0.1
	cavalry_weight = 0
	cavalry_cap = 0
    artillery_weight = 0.5
	sprites = { westerngfx_sprite_pack }
    trigger = {
		has_country_flag = aelnar_grand_army_4
	}
	cost_modifier = 0.33
	modifier = { 
		fire_damage_received = -0.2
		artillery_power = 0.2
		discipline = 0.1
	}
}

#Undead Precursor Legion
merc_aelnar_precursor_legions = {
    regiments_per_development = 0.5
	cavalry_weight = 0
	cavalry_cap = 0
    artillery_weight = 0
	sprites = { westerngfx_sprite_pack }
    trigger = {
		has_country_flag = aelnar_precursor_legion
	}
	cost_modifier = 0
	modifier = { 
		fire_damage_received = -1
		shock_damage_received = 1
		infantry_power = 0.5
		land_morale = 0.25
	}
}

#Nomsgaetir, unlocked via the Reverian MT (A05_gnomes_in_the_army)
merc_nomsgaetir = {
    regiments_per_development = 0.05
	cavalry_weight = 0
	cavalry_cap = 0
	artillery_weight = 0.3
	home_province = 177
	sprites = { westerngfx_sprite_pack }
     trigger = {
		has_country_flag = reveria_nomsgaetir_mercs_flag
		owns_core_province = 177
	}
	cost_modifier = 0.7
	modifier = { 
		movement_speed_onto_off_boat_modifier = 0.33 # These guys are sailors first and foremost (essentially marines)
		shock_damage = 0.1 # Scary Reaver tactics
		artillery_fire = 0.25 # Gnomish Artillery
	}
}

# Beastslayers - through MT
merc_castanor_beastslayers = {
	regiments_per_development = 0.025
	cavalry_weight = 0.1
	artillery_weight = 0
	cavalry_cap = 8
	sprites = { castanor_sprite_pack }
    trigger = {
		has_country_flag = beastslayer_company_unlocked
	}
	cost_modifier = 1.0
	modifier = { 
		shock_damage_received = -0.30 # Trained against centaur charges
		discipline = -0.05 # Highly irregular/unorthodox methods of fighting, adapted to beasts + not regular battles
		infantry_power = 0.15 # Highly effective fighters overall
		movement_speed = 0.1 # They be fast yo
		reinforce_speed = -0.5
	}
}

# Wardens of the White Walls - Through MT
merc_wardens_of_the_white_wall = {
    regiments_per_development = 0.01
    home_province = 755
	artillery_weight = 0.4
	sprites = { dlc028_ned_sprite_pack dlc042_ned_sprite_pack castanor_sprite_pack westerngfx_sprite_pack }
    trigger = {
		has_country_flag = wardens_of_the_white_walls_unlocked
	}
	cost_modifier = 0.9
	modifier = {
		movement_speed = 0.2
		reinforce_speed = -0.5
		fire_damage = 0.2
	}
}

# Castanorian Legions
# Dragonflame Legion
merc_castanorian_legion_1 = {
    regiments_per_development = 60
	cavalry_weight = 0.1
	cavalry_cap = 6
	artillery_weight = 0.34
	sprites = { dlc028_ned_sprite_pack dlc042_ned_sprite_pack castanor_sprite_pack westerngfx_sprite_pack }
    trigger = {
		has_country_flag = castanorian_legion_option_1_unlocked
		tag = B32
	}
	cost_modifier = 0
	modifier = {
		infantry_fire = 0.3
		fire_damage = 0.2
		discipline = 0.05
		infantry_shock = -0.5
		reinforce_speed = -0.5
	}
}
# Sword Legion
merc_castanorian_legion_2 = { 
    regiments_per_development = 60
	cavalry_weight = 0.1
	cavalry_cap = 6
	artillery_weight = 0.41
	sprites = { dlc028_ned_sprite_pack dlc042_ned_sprite_pack castanor_sprite_pack westerngfx_sprite_pack }
    trigger = {
		has_country_flag = castanorian_legion_option_2_unlocked
		tag = B32
	}
	cost_modifier = 0
	modifier = {
		land_morale = 0.1
		infantry_power = 0.2
		fire_damage_received = 0.20
		shock_damage_received = 0.20
		reinforce_speed = -0.5
	}
}
# Shield Legion
merc_castanorian_legion_3 = {
    regiments_per_development = 60
	cavalry_weight = 0.1
	cavalry_cap = 6
	artillery_weight = 0.41
	sprites = { dlc028_ned_sprite_pack dlc042_ned_sprite_pack castanor_sprite_pack westerngfx_sprite_pack }
    trigger = {
		has_country_flag = castanorian_legion_option_3_unlocked
		tag = B32
	}
	cost_modifier = 0
	modifier = {
		fire_damage_received = -0.3
		shock_damage_received = -0.3
		movement_speed = -0.1
		reinforce_speed = -0.5
	}
}
# Giantsbane Legion
merc_castanorian_legion_4 = {
    regiments_per_development = 60
	cavalry_weight = 0.1
	cavalry_cap = 6
	artillery_weight = 0.34
	sprites = { dlc028_ned_sprite_pack dlc042_ned_sprite_pack castanor_sprite_pack westerngfx_sprite_pack }
    trigger = {
		has_country_flag = castanorian_legion_option_4_unlocked
		tag = B32
	}
	cost_modifier = 0
	modifier = {
		discipline = 0.10
		land_attrition = -0.2
		shock_damage_received = -0.15
		cavalry_power = -0.15
		artillery_power = -0.10
		reinforce_speed = -0.5
	}
}
# Bridgeburners
merc_castanorian_legion_5 = {
    regiments_per_development = 60
	artillery_weight = 0.5
	sprites = { dlc028_ned_sprite_pack dlc042_ned_sprite_pack castanor_sprite_pack westerngfx_sprite_pack }
    trigger = {
		has_country_flag = castanorian_legion_option_5_unlocked
		tag = B32
	}
	cost_modifier = 0
	modifier = {
		artillery_fire = 0.1
		artillery_power = 0.1
		fire_damage = 0.15
		discipline = -0.05
		reinforce_speed = -0.5
	}
}
# Thundersworn
merc_castanorian_legion_6 = {
    regiments_per_development = 60
	cavalry_weight = 0.27
	cavalry_cap = 16
	artillery_weight = 0.34
	sprites = { dlc028_ned_sprite_pack dlc042_ned_sprite_pack castanor_sprite_pack westerngfx_sprite_pack }
    trigger = {
		has_country_flag = castanorian_legion_option_6_unlocked
		tag = B32
	}
	cost_modifier = 0
	modifier = {
		cavalry_power = 0.25
		cavalry_flanking = 0.5
		shock_damage = 0.25
		artillery_power = -0.1
		movement_speed = 0.1
		reinforce_speed = -0.5
	}
}

# Magebound
merc_castanorian_legion_7 = {
    regiments_per_development = 60
	cavalry_weight = 0.1
	cavalry_cap = 4
	sprites = { dlc028_ned_sprite_pack dlc042_ned_sprite_pack castanor_sprite_pack westerngfx_sprite_pack }
    trigger = {
		has_country_flag = castanorian_legion_option_7_unlocked
		tag = B32
	}
	cost_modifier = 0
	modifier = {
		infantry_shock = 0.2
		fire_damage_received = -0.1
		shock_damage_received = -0.1
		land_morale = -0.1
		discipline = 0.05
		reinforce_speed = -0.5
	}
}

# Hordebreaker
merc_castanorian_legion_8 = {
    regiments_per_development = 60
	artillery_weight = 0.42
	sprites = { dlc028_ned_sprite_pack dlc042_ned_sprite_pack castanor_sprite_pack westerngfx_sprite_pack }
    trigger = {
		has_country_flag = castanorian_legion_option_8_unlocked
		tag = B32
	}
	cost_modifier = 0
	modifier = {
		shock_damage_received = -0.3
		fire_damage_received = 0.1
		land_morale = 0.1
	}
}

#Mithril Company, unlocked via "The Mithril Company" mission (I05_prepare_company_title), and expanded later in it.
merc_mithril_company = {
    regiments_per_development = 0.05
	cavalry_weight = 0.1
	cavalry_cap = 2
    artillery_weight = 0.2
	sprites = { westerngfx_sprite_pack }
    trigger = {
		NOT = { has_global_flag = mithril_company_hired } #Allows it to only exist once.
		NOT = { is_rival = I05 }
		NOT = { tag = I05 }
		OR = {
			AND = {
				has_country_modifier = dwarven_administration
				has_global_flag = mithradhum_company_local
			}
			AND = {
				has_country_modifier = human_administration
				has_global_flag = mithradhum_company_extend
			}
			AND = {
				has_country_modifier = elven_administration
				has_global_flag = mithradhum_company_far
			}
			AND = {
				NOT = { has_country_modifier = monstrous_nation }
				has_global_flag = mithradhum_company_halanna
			}
		}
		OR = {
			AND = {
				capital_scope = {
					OR = {
						region = west_dwarovar_region
						region = serpentreach_region
					}
				}
				has_global_flag = mithradhum_company_local
			}
			AND = {
				capital_scope = {
					OR = {
						superregion = west_serpentspine_superregion
						superregion = escann_superregion
					}
				}
				has_global_flag = mithradhum_company_extend
			}
			AND = {
				capital_scope = {
					OR = {
						superregion = deepwoods_superregion
						region = tree_of_stone_region
					}
				}
				has_global_flag = mithradhum_company_far
			}
			AND = {
				capital_scope = {
					superregion = forbidden_lands_superregion
				}
				has_global_flag = mithradhum_company_plains
			}
			AND = {
				capital_scope = {
					OR = {
						superregion = bulwar_superregion
						region = jade_mines_region
					}
				}
				has_global_flag = mithradhum_company_halanna
			}
		}
	}
	cost_modifier = 0.45
	modifier = { 
		shock_damage = 0.05
		shock_damage_received = -0.1
	}
}

#Order of the Zenith
merc_order_of_the_zenith = {
    regiments_per_development = 0.025
	home_province = 596 #Zansap
	cavalry_weight = 0
	cavalry_cap = 0
    artillery_weight = 0.2
	sprites = { muslimgfx_sprite_pack }
    trigger = {
		has_country_flag = order_of_the_zenith
	}
	modifier = { 
		infantry_power = 0.10
		discipline = 0.05
	}
}

#Seinathil Warsingers
merc_seinathil_warsingers = {
	regiments_per_development = 0.05
	cavalry_weight = 0.1
	artillery_weight = 0
	cavalry_cap = 8
	home_province = 916 # Seinathil
	sprites = { easterngfx_sprite_pack }
    trigger = {
		has_country_flag = seinathil_mercs_unlocked
		owns_core_province = 916
	}
	cost_modifier = 0.70
	modifier = {
		shock_damage_received = -0.10 # Good support bards
		land_morale = 0.05 #Well trained
		may_recruit_female_generals = yes
        female_advisor_chance = 1.0
	}
}
